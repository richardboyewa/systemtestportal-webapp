**Acceptance criteria**  
#### General
- [ ] CI runs without problems  
- [ ] Migration scripts exist and work for existing data
- [ ] Code does contain license headers  

#### Code quality
- [ ] Code does not contain variables with abbreviated names
- [ ] Code does not contain TODOs, unused functions or console outputs  
- [ ] Code has been inspected and deemed to have decent quality  
- [ ] If this is a bug fix, a test covering the bug is present
- [ ] All javascript methods have been commented using Doc-comments(/** */)
- [ ] Tests for added backend code are present

#### Documentation
- [ ] If necessary, the changes are documented in the changelog  
- [ ] Newly used libraries have been added to the "about.tmpl" 
- [ ] If changes to the documentation are necessary (frontend changed or new frontend features were added), an issue for said changes has been created

[effectiveGo]: https://golang.org/doc/effective_go.html  