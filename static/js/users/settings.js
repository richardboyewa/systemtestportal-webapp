/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

const emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/**
 * Saves the account settings using a put request
 */
$("#saveButton").click(function() {
    const email = $("#inputNewEmail").val();
    const bio = $("#inputUserBio").val();
    const userPassword = $("#inputOldPassword").val();
    const newPassword = $("#inputNewPassword").val();
    const newPasswordRepeat = $("#inputNewPasswordRepeat").val();

    const isShownPublic = $("#inputIsShownPublic").prop('checked');
    const isEmailPublic = $("#inputIsEmailPublic").prop('checked');

    if((newPassword != "" || newPasswordRepeat != "")) {
        if (newPassword != newPasswordRepeat) {
            showError("The new password fields must match.");
            return;
        } else {
            if (newPassword.length < 8) {
                showError("The new password must be at least 8 characters long.");
                return;
            }
        }
    }
    if (!emailReg.test(email)) {
        showError("Please enter a valid email address.");
        return;
    }
    if (userPassword == "") {
        showError("You need to enter your current password to change your profile settings.");
        return;
    }

    $.ajax({
        url: currentURL(),
        type: "PUT",
        data: {
            "user": target,
            "email": email,
            "bio": bio,
            "isShownPublic": isShownPublic,
            "isEmailPublic": isEmailPublic,
            "userPassword": userPassword,
            "newPassword": newPassword,
        },
        statusCode: {
            200: function() {
                goToProfile();
            }
        }

    }).fail(response => {
        showError(response.responseText)
    });
});


function goToProfile() {
    window.location = currentURL().takeFirstSegments(currentURL().segments.length - 1)
}

function showError(error) {
    $("#errorModalResponseTextEditProfile").empty().append(error);
    $("#errorModal").modal("show");
}