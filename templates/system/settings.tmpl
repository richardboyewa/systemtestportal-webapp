{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}
{{define "content"}}

<!-- DATE TIME PICKER -->
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />

{{ if .SignedIn }}

        {{if .User.IsAdmin}}

            <h3>General Settings</h3>
            <p>Control how users access your STP</p>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputIsAccessAllowed">Allow general access</label>
                <div class="col-sm-10">
                    <input type="checkbox" id="inputIsAccessAllowed" name="inputIsAccessAllowed"
                           aria-describedby="inputIsAccessAllowedHelp"
                           {{if .SystemSettings.IsAccessAllowed}}
                           checked
                           {{end}}
                            checked onclick="return false;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                    <small id="inputIsAccessAllowedHelp" class="form-text text-muted">
                        This checkbox restricts users, which are not signed in, from viewing any contents of this STP
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputIsRegistrationAllowed">Allow registrations</label>
                <div class="col-sm-10">
                    <input type="checkbox" id="inputIsRegistrationAllowed" name="inputIsRegistrationAllowed"
                           aria-describedby="inputIsRegistrationAllowedHelp"
                            {{if .SystemSettings.IsRegistrationAllowed}}
                           checked
                            {{end}}>
                    <small id="inputIsRegistrationAllowedHelp" class="form-text text-muted">
                        This checkbox allows users to register on this STP without an Admin manually adding them
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputIsEmailVerification">Require email verification</label>
                <div class="col-sm-10">
                    <input type="checkbox" id="inputIsEmailVerification" name="inputIsEmailVerification"
                           aria-describedby="inputIsEmailVerificationHelp"
                            {{if .SystemSettings.IsEmailVerification}}
                            checked
                            {{end}}
                           onclick="return false;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                    <small id="inputIsEmailVerificationHelp" class="form-text text-muted">
                        This checkbox requires users to perform email verification upon registration
                    </small>
                </div>
            </div>

            <h3>Global message</h3>
            <p>Display a message for all users of your STP</p>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputIsDisplayMessage">Display global message</label>
                <div class="col-sm-10">
                    <input type="checkbox" id="inputIsDisplayMessage" name="inputIsDisplayMessage"
                           aria-describedby="inputIsEmailVerificationHelp"
                            {{if .SystemSettings.IsDisplayGlobalMessage}}
                            checked
                            {{end}}>
                    <small id="inputIsEmailVerificationHelp" class="form-text text-muted">
                        This checkbox makes users see a global message anywhere on this STP.
                    </small>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputGlobalMessage">Global mesage</label>
                <div class="col-sm-10">
                    <textarea class="form-control markdown-textarea" id="inputGlobalMessage" style="resize: none;"
                              name="inputGlobalMessage" aria-describedby="inputGlobalMessageHelp"
                              placeholder="e.g. 'Maintenance from 26/07/18 to 28/07/18'"
                              rows="4" maxlength="250">{{.SystemSettings.GlobalMessage}}</textarea>

                    <small id="inputGlobalMessageHelp" class="form-text text-muted">
                        This text will displayed in the global message ( Max length: 250 characters)
                    </small>
                </div>
            </div>

            <div class="form-group row">
                <div class="form-group p-3">
                    <label><strong>Message type</strong></label>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="inputGlobalMessageType"
                                   id="optionNotification" value="system-message-notification"
                                    {{if eq .SystemSettings.GlobalMessageType "system-message-notification"}}
                                    checked
                                    {{else if eq .SystemSettings.GlobalMessageType ""}}
                                    checked
                                    {{end}}>
                            <b>Notification</b> &mdash; Displayed in an non-intrusive blue header
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="inputGlobalMessageType"
                                   id="optionWarning" value="system-message-warning"
                                    {{if eq .SystemSettings.GlobalMessageType "system-message-warning"}}
                                    checked
                                    {{end}}>
                            <b>Warning</b> &mdash; Displayed in an intrusive orange header
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputIsGlobalMessageExpirationDate">Expiration Date</label>
                <div class="col-sm-10">
                    <input type="checkbox" id="inputIsGlobalMessageExpirationDate" name="inputIsGlobalMessageExpirationDate"
                           aria-describedby="inputIsGlobalMessageExpirationDateHelp"
                    {{if .SystemSettings.IsGlobalMessageExpirationDate}}
                           checked
                    {{end}}>
                    <small id="inputIsGlobalMessageExpirationDateHelp" class="form-text text-muted">
                        This checkbox makes the global message no longer display when the given date is reached.
                    </small>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-2 col-form-label" for="inputGlobalMessageExpirationDate">Date to expire on</label>
                <div class="col-sm-10">
                    <input id="datepicker" width="276"
                           {{if not .SystemSettings.GlobalMessageExpirationDate.IsZero}}
                                value="{{.SystemSettings.GetExpirationDateForDatePicker}}"
                           {{end}}/>
                    <script>
                        $('#datepicker').datepicker({
                            uiLibrary: 'bootstrap4'
                        });
                    </script>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10 ml-auto">
                    <button class="btn btn-success" id="saveButton">Save</button>
                </div>
            </div>

            <div class="modal fade" id="errorModal">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"> Error when saving settings </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p id="errorModelResponseTextSystemSettings"></p>
                        </div>
                        <div class="modal-footer">
                            <button id="buttonClose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <script src="/static/js/util/common.js"></script>
            <script src="/static/js/system/settings.js"></script>



        {{else}}

            <div class="jumbotron jumbotron" style="background-color:transparent;">
                <div class="container">
                    <h1 class="display-4">System settings are not available</h1>
                    <hr class="my-4">
                    <p class="lead">You do not have the right permissions.</p>
                </div>
            </div>

        {{end}}

    {{else}}

        <div class="jumbotron jumbotron" style="background-color:transparent;">
            <div class="container">
                <h1 class="display-4">System settings are not available</h1>
                <hr class="my-4">
                <p class="lead">You do not have the right permissions.</p>
            </div>
        </div>

    {{end}}

{{end}}