{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
{{$parent := .}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Test Sequence Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            This shows the details of a test sequence.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Buttons</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Back</span>
                            </button>
                        </td>
                        <td>Get back to the test sequence list.</td>
                    </tr>
                {{ if .ProjectPermissions.DeleteSequence }}
                    <tr>
                        <td>
                            <button class="btn btn-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Delete</span>
                            </button>
                        </td>
                        <td>Open a confirm dialog before deleting the current test sequence.</td>
                    </tr>
                {{ end }}
                {{ if .ProjectPermissions.DuplicateSequence }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="duplicate">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Duplicate</span>
                            </button>
                        </td>
                        <td>Duplicate the test sequence with history up to the currently selected version.</td>
                    </tr>
                {{ end }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="history">
                                <i class="fa fa-history" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> History</span>
                            </button>
                        </td>
                        <td>Show a list with all previous versions of this test sequence.</td>
                    </tr>
                {{ if .ProjectPermissions.EditSequence }}
                    {{ if (ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="revert">
                                <abbr title="Reverts the test case to the selected version">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline">Revert</span>
                                </abbr>
                            </button>
                        </td>
                        <td>Revert the test sequence to the selected version.</td>
                    </tr>
                    {{ else }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="edit">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Edit</span>
                            </button>
                        </td>
                        <td>Edit the current test sequence.</td>
                    </tr>
                    {{ end }}
                {{ end }}
                {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" disabled>
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Assignees</span>
                            </button>
                        </td>
                        <td>Assign this test sequence as a task to a team member.</td>
                    </tr>
                {{ end }}
                {{ if and .ProjectPermissions.Execute (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-success" disabled>
                                <i class="fa fa-play" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Start</span>
                            </button>
                        </td>
                        <td>Start the execution of the current test sequence.</td>
                    </tr>
                {{ end }}
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

{{template "delete-pop-up" .DeleteTestSequence}}

<div class="modal fade" id="revertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Revert Test Sequence</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you really want to revert the test sequence to this version?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonRevertConfirmed">Revert</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="duplicateModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-clone"></i> Duplicate Test Sequence</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>This duplicates the current test sequence up to the currently shown version.
                    The test sequence duplicate needs a unique name.</p>
                <input class="form-control" id="inputDuplicateTestSequenceName"
                       placeholder="Enter name for test sequence duplicate" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonDuplicateConfirmed">Duplicate</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{ template "modal-tester-assignment" . }}
<div class="tab-card card" id="tabTestsequences">
    <nav class="navbar navbar-light action-bar p-3 d-print-none">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Back</span>
            </button>
        {{ if .ProjectPermissions.DeleteSequence }}
            <button class="btn btn-danger mr-2" id="buttonDelete" data-toggle="modal"
                    data-target="#deleteTestSequence">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Delete</span>
            </button>
        {{ end }}
        {{ if .ProjectPermissions.EditSequence }}
            <button class="btn btn-primary mr-2" data-toggle="modal"
                    id="buttonDuplicate" formaction="duplicate" data-target="#duplicateModal">
                <i class="fa fa-clone" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Duplicate</span>
            </button>
        {{ end }}
            <button class="btn btn-primary mr-2" id="buttonHistory" formaction="history">
                <i class="fa fa-history" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> History</span>
            </button>
        {{ if .ProjectPermissions.EditSequence }}
        {{ if (ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2" formaction="revert" data-toggle="modal"
                    data-target="#revertModal">
                <abbr title="Reverts the test case to the selected version">
                    <i class="fa fa-undo" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline">Revert</span>
                </abbr>
            </button>
        {{ else }}
            <button class="btn btn-primary mr-2" id="buttonEdit" formaction="edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Edit</span>
            </button>
        {{ end }}
        {{ end }}
        {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 float-right btn-sm" id="buttonAssign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Assignees</span>
            </button>
        {{ end }}
        {{ if and .ProjectPermissions.Execute (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr)}}
        {{ with .TestSequenceVersion }}
        {{ if .Cases }}
        <button class="btn btn-success" id="buttonExecute">
        {{ else }}
        <button class="btn btn-success" id="buttonExecute" disabled data-toggle="tooltip" data-placement="bottom"
                title="" data-original-title="This test sequence has no cases!">
        {{ end }}
            <i class="fa fa-play" aria-hidden="true"></i>
            <span class="d-none d-md-inline"> Start</span>
        </button>
        {{ end }}
        {{ end }}
        </div>
    </nav>
    <div class="row tab-side-bar-row">
    <div class="col-md-9 p-3">
    {{ if ne .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr }}
        <div class="alert alert-warning alert-dismissible">
            <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning!</strong> You are viewing an older version of the test sequence
        </div>
    {{ end }}
        <h4 class="mb-3">{{ .TestSequence.Name }}</h4>
    {{ with .TestSequenceVersion }}
        <div class="form-group">
            <label><strong>Test Sequence Description</strong></label>
            <div class="text-muted">
            {{ with .Description }}
                        {{printMarkdown . }}
                    {{ else }}
                No Description
            {{ end }}
            </div>
        </div>
        <div class="form-group">
            <label><strong>Test Sequence Preconditions</strong></label>
            <ul class="list-group" id="preconditionsList">
            {{ if .Preconditions }}
            {{ range .Preconditions }}
                <li class="list-group-item preconditionItem">
                    <span>{{ .Content }}</span>
                </li>
            {{ end }}
            {{ else }}
                <li class="list-group-item">
                    <span>No preconditions.</span>
                </li>
            {{ end }}
            </ul>
        </div>
        <div class="form-group">
            <label><strong>Test Cases</strong></label>
        {{ if .Cases }}
            <ul class="list-group">
            {{ range .Cases }}
                <li class="list-group-item testCaseLine" id="{{ .Name }}">
                {{ .Name }}
                </li>
            {{ end }}
            </ul>
        {{ else }}
            <p class="text-muted">No Test Cases</p>
        {{ end }}
        </div>
    {{template "comments" $parent}}
    </div>
    <div class="col-md-3 p-3 tab-side-bar">
        <div class="form-group">
            <label><strong>Applicability</strong></label>
        {{ if .SequenceInfo.Versions }}
            <div class="form-group">
                <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                    <label for="inputTestCaseSUTVersions">
                        <strong>Versions</strong>
                    </label>
                </div>
                <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                    <select class="custom-select mb-2" id="inputTestCaseSUTVersions">

                    </select>
                </div>
                <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                    <label for="inputTestCaseSUTVariants">
                        <strong>Variants</strong>
                    </label>
                </div>
                <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                    <ul class="list-group" id="inputTestCaseSUTVariants">
                    </ul>
                </div>
            </div>
        {{ else }}
            <div class="form-group">
                <p class="text-muted">This Test Sequence is not applicable to any system-under-test-versions.
                    Edit the Test Sequence and select applicable versions.</p>
            </div>
        {{ end }}
        </div>
        <div class="form-group">
            <label><strong>Estimated Test Duration</strong></label>
            <p class="text-muted">
            {{ if and (eq .SequenceInfo.DurationHours 0) (eq .SequenceInfo.DurationMin 0) }}
                No Test Duration
            {{ else }}
            {{ if gt .SequenceInfo.DurationHours 0 }}
            {{ .SequenceInfo.DurationHours }}
            {{ if eq .SequenceInfo.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
            {{ end }}
            {{ if gt .SequenceInfo.DurationMin 0 }}
            {{ .SequenceInfo.DurationMin }}
            {{ if eq .SequenceInfo.DurationMin 1 }}
                Minute
            {{ else }}
                Minutes
            {{ end }}
            {{ end }}
            {{ end }}
            </p>
        </div>
        <div class="form-group">
            <label><strong>Number of Test Cases</strong></label>
            <p class="text-muted">
            {{ len .Cases }}
            </p>
        </div>
    {{ end }}
    {{ $projectLabels := .Project.Labels }}
    {{ with .TestSequence }}
        <div class="form-group">
            <label><strong>Labels</strong></label>
            <select id="btnEditLabels" class="btn-link" multiple="multiple">
            {{ range $indexProject, $labelProject := $projectLabels }}
                <option value="{{ $indexProject }}">{{ $labelProject.Name }}</option>
            {{ end }}
            </select>
            <p id="labelContainer">
            {{ range .Labels }}
                <span class="badge badge-primary" data-toggle="tooltip" title="{{ .Description }}"
                      style="background-color: #{{ .Color }};">{{ .Name }}</span>
            {{ else }}
                <span class="text-muted">No Labels</span>
            {{ end }}
            </p>
        </div>
    {{ end }}
    <div class="form-group">
        <label><strong>Assignees</strong></label>
        {{ if and .ProjectPermissions.AssignSequence (eq .TestSequenceVersion.VersionNr (index .TestSequence.SequenceVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 float-right btn-sm" id="buttonAssign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
            </button>
        {{ end }}
        <div id="testerContainer">
             {{if .Tasks }}
              <ul class="list-group">
                {{ range .Tasks }}
                 <li class="list-group-item" id="task-{{ .Assignee }}-{{ .Index }}">
                    <span class="text-muted">{{ .Assignee }}</span>
                    <span class="text-muted">{{ .Version }}</span>
                    <span class="text-muted">{{ .Variant }}</span>
                 </li>
                {{ end }}
              </ul>
            {{ else }}
                <ul>
                </ul>
                <span class="text-muted">No assigned testers</span>
            {{ end }}
        </div>
    </div>
    </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/sut-versions-show.js"></script>
<script src="/static/js/util/common.js"></script>
<script src="/static/js/project/testcases.js"></script>
<script src="/static/js/project/testsequences.js"></script>
<script src="/static/js/project/labels.js"></script>

<script type='text/javascript'>
    // Listen to changes in the drop down menu of the variants
    setVersionOnChangeListener('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');


    // Send a request to get the sut variants and versions
    var url = getTestURL().appendSegment("json").toString();

    // Request test case to get the sut versions from it
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            testObjectVersionData = {{ .SequenceInfo.Versions }}
            if (testObjectVersionData != null) {
                // Fill the drop down menu with the variants
                fillVersions(testObjectVersionData, "#inputTestCaseSUTVersions");
                // Update the version list with the versions of the selected variant
                updateVariantShowList('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');
            }
        }
    };

    $("#buttonRevertConfirmed").click(revertTestSequence);

    // Reverts to selected older version of a testsequence.
    function revertTestSequence() {
        $('#revertModal').on('hidden.bs.modal', revertAjaxRequest).modal('hide');
    }

    $('#buttonDuplicateConfirmed').off('click').click(function () {

        $('#duplicateModal').off('click').one('hidden.bs.modal', function (e) {
            e.preventDefault();

            var url = currentURL().appendSegment("duplicate").toString() + "?fragment=true";

            var posting = $.post(url, {
                inputTestSequenceName: $('#inputDuplicateTestSequenceName').val(),
                version: "{{ .TestSequenceVersion.VersionNr }}"
            });

            posting.done(function (response) {
                var url = currentURL()
                        .takeFirstSegments(4)
                        .appendCodedSegment(posting.getResponseHeader("newName"))
                        .toString();

                $('#tabTestsequences').empty().append(response);
                history.pushState('data', '', url);
            }).fail(function (response) {
                $("#modalPlaceholder").empty().append(response.responseText);
                $('#errorModal').modal('show');
            });
        }).modal('hide');
    });

    function revertAjaxRequest(e) {
        e.preventDefault();

        /* Send the data using post with element ids*/
        var posting = $.ajax({
            url: currentURL().appendSegment("update").toString() + "?fragment=true",
            type: "PUT",
            data: getRevertData()
        });

        /* Alerts the results */
        posting.done(function (response) {
            var url = currentURL()
                    .takeFirstSegments(4)
                    .appendCodedSegment(posting.getResponseHeader("newName"))
                    .toString();

            $('#tabTestsequences').empty().append(response);
            history.pushState('data', '', url);
        }).fail(function (response) {
            $("#modalPlaceholder").empty().append(response.responseText);
            $('#errorModal').modal('show');
        });
    }

    function getRevertData() {
        return {
            //{{ with .TestSequenceVersion }}
            version: "{{ .VersionNr }}",
            inputCommitMessage: "Revert test case to version {{ .VersionNr }}",
            inputTestSequenceDescription: "{{ .Description }}",
            inputTestSequencePreconditions: "{{ .Preconditions }}",
            //{{ end }}
            //{{ with .TestSequence }}
            inputTestSequenceName: "{{ .Name }}"
            //{{ end }}
        }
    }

    $("#printerIcon").removeClass("d-none");

    function printer() {
        window.open(currentURL().appendSegment('print').toString(),
                'newwindow',
                'width=600,height=800');
        return false;
    }
</script>

<script type="text/javascript">
    assignButtonsTestSequence();

    $(".testCaseLine").click(showTestCaseVersionFromTestSequence);

    // testSequenceLabels is an array with all the labels of the test sequence
    var testSequenceLabels = {{ .TestSequence.Labels }};
    if (testSequenceLabels === null) {
        testSequenceLabels = [];
    }

    // Save the labels of project
    // It's needed to reference the correct label when changing the
    // labels of the test sequence
    var projectLabels = {{ .Project.Labels }};

    $(document).ready(function () {
        var btnEditLabels = $('#btnEditLabels');

        btnEditLabels.multiselect({
            buttonClass: 'btn btn-link',
            buttonText: function (options, select) {
                return 'Edit';
            },
            // Send request to server to update labels of test sequence
            onDropdownHide: function (event) {
                // The url to send the update request to
                var urlLabelsPost = getTestURL().appendSegment("labels").toString();

                var xhr = new XMLHttpRequest();
                xhr.open("POST", urlLabelsPost, true);
                xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

                // Clear the array with the old labels of the test sequence
                testSequenceLabels.length = 0;
                // Save the newly selected labels in the array with labels
                btnEditLabels.val().forEach(function (labelIndex) {
                    testSequenceLabels.push(projectLabels[labelIndex])
                });

                // Send the array of labels as JSON to the server
                xhr.send(JSON.stringify(testSequenceLabels));

                xhr.onloadend = function () {
                    if (this.status !== 200) {
                        //console.log(this.response);
                    }
                };
                // Update labels of test sequence in gui
                showLabels(testSequenceLabels, "#labelContainer");
            }
        });
        btnEditLabels.multiselect('select', getIndexOfTestSequenceLabels());
    });

    // getIndexOfTestSequenceLabels returns the indices of the
    // labels of the test sequence. These labels are selected in
    // the labels-dropdown.
    function getIndexOfTestSequenceLabels() {
        var indexList = [];
        $.each(testSequenceLabels, (function (key, testSequenceLabel) {
            $.each(projectLabels, (function (projectLabelIndex, projectLabel) {
                if (testSequenceLabel.Name === projectLabel.Name) {
                    indexList.push(projectLabelIndex);
                }
            }));
        }));
        return indexList;
    }
</script>
{{end}}
