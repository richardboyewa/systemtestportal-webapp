{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<link rel="stylesheet" type="text/css" href="/static/css/project/reorder-utils.css">

<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Test Case</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            You can fill in all the relevant data for a new test sequence in this form.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Buttons</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Abort</span>
                            </button>
                        </td>
                        <td>Aborts the creation process.</td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-success">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Save</span>
                            </button>
                        </td>
                        <td>Confirms the form content and proceeds with creation.</td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Form Fields</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Sequence Name</strong>
                        </td>
                        <td>Enter a short but descriptive name for the test sequence.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Sequence Description</strong>
                        </td>
                        <td>Describe the purpose of that test sequence in some sentences.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Sequence Preconditions</strong>
                        </td>
                        <td>State some preconditions that must be fulfilled prior to test sequence execution.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Cases</strong>
                        </td>
                        <td>Add some test cases here.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Versions & Variants</strong>
                        </td>
                        <td>Based on your selected test cases, these are automatically calculated.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Estimated Test Time (hh:mm)</strong>
                        </td>
                        <td>Based on your selected test cases, it's automatically calculated .</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestSequences">
    <form id="testSequenceEdit">

        <!-- Control buttons -->
        <nav class="navbar navbar-light action-bar p-3">
            <div class="input-group flex-nowrap">
                <button class="btn btn-secondary" id="buttonAbort" type="button">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> Abort</span>
                </button>
                <button id="buttonSaveTestSequence" class="btn btn-success ml-2 mouse-hover-pointer" type="submit">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> Save</span>
                </button>
            </div>
        </nav>

        <!-- Text inputs -->
        <div class="row tab-side-bar-row">
            <div class="col-md-9 p-3">
                <h4 class="mb-3">Create New Test Sequence</h4>
                <div class="form-group">
                    <label for="inputTestSequenceName"><strong>Test Sequence Name</strong></label>
                    <input id="inputTestSequenceName"
                           class="form-control"
                           placeholder="Enter test sequence name"
                           pattern=".{4,}"
                           oninput="this.setCustomValidity('')"
                           oninvalid="this.setCustomValidity('Please enter at least 4 characters.')"
                           required>
                </div>
                <div class="form-group">
                    <label for="inputTestSequenceDescription"><strong>Test Sequence Description</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestSequenceDescription" rows="4"
                                  placeholder="Describe the test sequence, e.g. 'This test sequence tests ticket buy options'"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">Markdown supported</div>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="inputTestSequencePreconditions"><strong>Test Sequence Preconditions</strong></label>
                    <ul class="list-group" id="preconditionsList">
                     {{ if .Preconditions }} 
                        {{ range $index, $elem := .Preconditions }}
                        <li class="list-group-item preconditionItem" >
                            <span>{{ $elem.Content }}</span>
                            <button class="close ml-2 list-line-item btn-sm deletePrecondition pull-right" type="button">
                                    <span class="d-none d-sm-inline">x</span>
                            </button>
                        </li>
                        {{ end }}
                    {{ end }} 
                    </ul>
                    <div class="input-group">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="preconditionAdder">
                            <i class="fa fa-plus"></i>
                        </button>
                        </span>
                        <input class="form-control width100" id="preconditionInput" placeholder="New Precondition">
                    </div>
                </div>
                <label class="mt-2 mb-0"><strong>Test Cases</strong></label>
                <button type="button" class="btn btn-sm btn-primary float-right" id="buttonAddTestcase"
                        data-toggle="modal" data-target="#modal-testcase-selection">
                    Add Test Case
                </button>
                <div id="sortable-container" class="form-group">
                    <ul id="testCasesList" class="list-group sortable pb-2 pt-2">
                        <li class="list-group-item" id="testCaseListPlaceholder">
                            <span class="text-muted">No Test Cases</span>
                            <a type="button" class="add-test-case close ml-auto" aria-label="Add TestCase"
                               data-toggle="modal" data-target="#modal-testcase-selection">
                                <span aria-hidden="true">+</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 p-3 tab-side-bar">
                <div class="form-group mt-4">
                    <label for="testSequenceTime"><strong>Estimated Test Duration</strong></label>
                    <p class="text-muted" id="testsequenceTime">
                         No Test Duration
                    </p>
                </div>
                <div class="form-group">
                    <label for="labels"><strong>Labels</strong></label>
                    <div class="row">
                        <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                            <select id="labels" multiple="multiple">
                            {{ range $key, $value := .Project.Labels }}
                                <option value="{{ $key }}">{{ $value.Name }}</option>
                            {{ end }}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            {{template "modal-testcase-selection" . }}
        </div>
    </form>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testsequences.js"></script>
<script src="/static/js/project/tests.js"></script>

<!-- Initialize bootstrap-multiselect-->
<script type="text/javascript">
    assignEventsToNameTextField();
    assignButtonsTestSequence();
    assignPreconditionInputListener();
    //save labels of project
    var projectLabels = {{ .Project.Labels }}
            // Initialize bootstrap multiselect
            $(document).ready(function() {
                $('#labels').multiselect();
            });
</script>
{{end}}


