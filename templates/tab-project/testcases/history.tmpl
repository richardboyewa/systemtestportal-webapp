{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{ define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Test Case History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            This shows a list of all versions of a test case.
                            Click on a row to view the details of the selected version.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestCases">
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary" id="buttonBackToCase">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Back</span>
            </button>
        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-12 p-3">
            <h4 class="mb-3">History of {{ .TestCase.Name }}</h4>
            <div class="form-group">
                <ul class="list-group" id="commitHistory">
                    {{ range .TestCase.TestCaseVersions }}
                    <li class="list-group-item versionLine" id="{{ .VersionNr }}">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-auto">
                                    <abbr title="Test Case Version">
                                        <span class="badge badge-primary mr-3">
                                        {{ .VersionNr }}
                                        </span>
                                    </abbr>
                                </div>
                                <div class="col">
                                    {{ if eq .Message "" }}
                                    <div class="text-muted">No commit message</div>
                                    {{ else }}
                                    {{ .Message }}
                                    {{ end }}
                                </div>
                                <div class="col-md-auto mr-4 mt-2 mt-md-0 mb-2 mb-md-0">
                                    {{ if eq .IsMinor true }}
                                    <span class="badge badge-info float-none float-md-right"><abbr
                                            title="This was a minor edit">Minor</abbr>
                                            </span>
                                    {{ end }}
                                </div>
                                <div class="col-md-auto">
                                    <abbr title="{{ .CreationDate.Format " Jan 02, 2006 15:04:05 UTC" }}">
                                    <span class="text-muted"><time class="timeagopreparation" datetime="">{{ .CreationDate }}</time></span>
                                    </abbr>
                                </div>
                            </div>
                        </div>
                    </li>
                    {{ end }}
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testcases.js"></script>

<script>
    assignButtonsTestCase();

    $('.timeago').each(function(i, obj) {
        var timeString = $(this).text();
        timeString = timeString.replace(" ", 'T');
        timeString = timeString.replace(" ", '');
        timeString = timeString.replace(" CET", '');
        $(this).text(timeString);
        $(this).attr("datetime", timeString);
    });
    jQuery("time.timeago").timeago();
</script>
{{ end }}