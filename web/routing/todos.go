/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/task"
)

func registerTaskHandler(authRouter *httptreemux.ContextMux) {
	taskStore := store.GetTaskStore()
	// _ := store.GetProjectStore()

	authRouter.Handler(http.MethodGet, Dashboard+Tasks+Opened, task.GetTask(taskStore, true))
	authRouter.Handler(http.MethodGet, Dashboard+Tasks+Closed, task.GetTask(taskStore, false))
	authRouter.Handler(http.MethodPut, Dashboard+Tasks+Opened, task.ItemPut(taskStore, taskStore))
	authRouter.Handler(http.MethodPut, Dashboard+Tasks+Closed, task.ItemPut(taskStore, taskStore))

	authRouter.Handler(http.MethodGet, Dashboard+Tasks+Opened+Async, task.GetAsyncTaskTab(taskStore, true))
	authRouter.Handler(http.MethodGet, Dashboard+Tasks+Closed+Async, task.GetAsyncTaskTab(taskStore, false))
}
