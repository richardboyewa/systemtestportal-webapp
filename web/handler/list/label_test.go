/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

var lableGroup = []project.Label{
	{
		Name:        "High Priority",
		Description: "This test has a high priority",
	},
	{
		Name:        "Low Priority",
		Description: "This test has a low priority",
	},
	{
		Name:        "Elepant",
		Description: "This test has an elepant priority",
	},
	{
		Name:        "Verschiedene",
		Description: "Huehuehuehue",
	},
	{
		Name:        "Obstsalat",
		Description: "Schlupp",
	},
}

var lableGroupSupset = []project.Label{
	{
		Name:        "High Priority",
		Description: "This test has a high priority",
	},
	{
		Name:        "Low Priority",
		Description: "This test has a low priority",
	},
	{
		Name:        "Elepant",
		Description: "This test has a low priority",
	},
}

var lableGroupSupsetWithAdditionalLable = []project.Label{
	{
		Name:        "High Priority",
		Description: "This test has a high priority",
	},
	{
		Name:        "Low Priority",
		Description: "This test has a low priority",
	},
	{
		Name:        "Wrong",
		Description: "This test has a low priority",
	},
}

var emptyLables = []project.Label{}

func TestContainsLabels_whenAllLablesAreContainedinGroup(t *testing.T) {
	expectedResult := true
	actualResult := containsLabels(lableGroupSupset, &lableGroup)

	if expectedResult != actualResult {
		t.Fatal("Expected true but got false")
	}

}

func TestContainsLabels_whenSomeLablesAreNotContained(t *testing.T) {
	expectedResult := false
	actualResult := containsLabels(lableGroupSupsetWithAdditionalLable, &lableGroup)

	if expectedResult != actualResult {
		t.Fatal("Expected false but got true")
	}
}

func TestContainsLabels_whenLableCheckHasMoreElementThanLableSet(t *testing.T) {
	expectedResult := false
	actualResult := containsLabels(lableGroup, &lableGroupSupset)

	if expectedResult != actualResult {
		t.Fatal("Expected false but got true")
	}
}

func TestContainsLabels_whenLableCheckIsEmpty(t *testing.T) {
	expectedResult := true
	actualResult := containsLabels(emptyLables, &lableGroup)

	if expectedResult != actualResult {
		t.Fatal("Expected false but got true")
	}
}

func TestContainsLabels_whenLableSetIsEmpty(t *testing.T) {
	expectedResult := false
	actualResult := containsLabels(lableGroup, &emptyLables)

	if expectedResult != actualResult {
		t.Fatal("Expected false but got true")
	}
}
