/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package display

import (
	"testing"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestParseMarkdown(t *testing.T) {
	body := "{\"text\":\"**This** is a *markdown* test!\"}"
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				ParseMarkdown,
				handler.HasStatus(http.StatusOK),
			),
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
			/*
					handler.CreateTest("Normal case",
			handler.ExpectResponse(
				ParseMarkdown,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, handler.NoParams),
		),
			handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusSeeOther),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
			 */
	)
}

func TestSanitize(t *testing.T) {
	body := "{\"text\":\"<strong>Test</strong><script>alert('XSS');<script>\"}"
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				ParseMarkdown,
				handler.HasStatus(http.StatusOK),
			),
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
	)
}
