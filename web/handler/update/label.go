/*
This file is part of SystemTestPortal.
Copyright (C) 2018  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"encoding/json"
)

const (
	errCouldNotDecodeLabelsTitle = "Couldn't save labels."
	errCouldNotDecodeLabels      = "We were unable to decode the change to labels " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

type labelInput [] struct {
	Name        string
	Description string
	Color       string
}

/*
 * updates a project label
 * expects the old label and the new label
 */
func ProjectLabelsPut(ps handler.ProjectLabelUpdater) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}
		if c.User == nil || !c.Project.GetPermissions(c.User).EditCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
		}
		input := labelInput{}
		if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeLabelsTitle, errCouldNotDecodeLabels, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}
		if len(input) != 2{
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeLabelsTitle, errCouldNotDecodeLabels, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}
		// input[0] is the old label data, input[1] is the new label data
		if err := ps.RenameProjectLabel(c.Project, input[0], input[1]); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
