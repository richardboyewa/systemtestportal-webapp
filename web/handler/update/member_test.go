/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestMemberUpdate(t *testing.T) {

	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    nil,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	paramsList := url.Values{}
	paramsList.Add(httputil.Members, "[\"benweiss\"]")
	paramsList.Add(httputil.Roles, "[\"Supervisor\"]")

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				userRetriever := &handler.UserRetrieverMock{}
				projectAdder := &handler.ProjectAdderMock{}

				return MemberUpdate(userRetriever, projectAdder),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodPatch, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodPatch, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				userRetriever := &handler.UserRetrieverMock{}
				projectAdder := &handler.ProjectAdderMock{}

				return MemberUpdate(userRetriever, projectAdder),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
					)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodPatch, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodPatch, handler.NoParams),
		),
	)
}
