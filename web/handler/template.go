/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"time"
	"github.com/nicksnyder/go-i18n/i18n"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"gitlab.com/stp-team/systemtestportal-webapp/web/util"
	"golang.org/x/text/language"
)

// IssueTracker is a html link to the issue board of the stp-repository
const IssueTracker = "<a href='https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker</a>"

const (
	errPrintTemplateTitle = "We were unable to print the page you requested!"
	errPrintTemplate      = "We are sorry, but we were unable to build the page " +
		"you wanted to see. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
)

// PrintTmpl prints given template with given context into given writer.
// If an error occurs, an error response is written to the writer instead
// of the executed template's content.
//
// If the given writer was a http.ResponseWriter the status code
// http.StatusInternalServerError will be written to its header if an
// error occurred.
func PrintTmpl(ctx context.Context, template *template.Template, w io.Writer, r *http.Request) {

	b := &bytes.Buffer{}
	err := template.Execute(b, ctx)
	if err == nil {
		_, err = b.WriteTo(w)
	}
	if err != nil {
		tmplErr := errors.ConstructStd(http.StatusInternalServerError,
			errPrintTemplateTitle, errPrintTemplate, r).
			WithLogf("Unable to print template %q.", template.Name()).
			WithStackTrace(2).
			WithCause(err).
			Finish()
		if rw, ok := w.(http.ResponseWriter); ok {
			errors.Respond(tmplErr, rw)
		} else {
			errors.Log(tmplErr)
			_, err = fmt.Fprint(w, tmplErr.Body())
			if err != nil {
				log.Panicf("Couldn't write error body to writer. "+
					"Error body was:\n%s", tmplErr.Body())
			}
		}
	}
}

func getLanguageFromRequest(r *http.Request) string {
	matcher := language.NewMatcher([]language.Tag{
		language.AmericanEnglish,
		language.German,
	})

	accept := r.Header.Get("Accept-Language")
	tag, _ := language.MatchStrings(matcher, accept)

	return tag.String()
}

func printMarkdown(s string) template.HTML {
	return template.HTML(util.ParseMarkdown(s))
}

func toIso8601Time(time time.Time) string {
	return time.UTC().Format("2006-01-02T15:04:05-0700")
}

// GetBaseTree returns a loaded base template containing
// only utility function used by the templates.
func GetBaseTree(r *http.Request) templates.LoadedTemplate {
	//lang := getLanguageFromRequest(r)

	T, _ := i18n.Tfunc("en-US")

	defaultFuncMap := map[string]interface{}{
		"printMarkdown": printMarkdown,
		"provideTimeago": toIso8601Time,
		"T":             T,
	}

	return templates.Load().Funcs(defaultFuncMap)
}

// GetSideBarTree returns a loaded template structure for a
// view containing the sidebar.
func GetSideBarTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append(templates.Header, templates.Navbar, templates.Footer). // Navbar tree
		// Modal sign-in tree
		Append(templates.SignIn).
		// Sidebar tree
		Append(templates.ContentSidebar, templates.MenuExplore)
}

// GetPrintTree returns a loaded template structure for a
// print view .
func GetPrintTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append("static/header-print", "static/navbar-print", "static/footer-print"). // Navbar tree
		// Sidebar tree
		Append("static/content-print")
}

// GetNoSideBarTree returns a loaded template structure for a
// view not containing the sidebar.
func GetNoSideBarTree(r *http.Request) templates.LoadedTemplate {
	return GetBaseTree(r).Append(templates.Header, templates.Navbar, templates.Footer). // Navbar tree
		// Modal sign-in tree
		Append(templates.SignIn).
		// No sidebar tree
		Append(templates.ContentNoSidebar)
}
