/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package ci_integration

import (
	"archive/zip"
	"bytes"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
)

// analyseProtocols analyses the protocols of the given sut-version.
//
// The tests in the stp are classified as follows:
// Not executed tests: Failed
// Executed tests can have different results:
//	- Passed: Passed
//	- Passed with comment: Passed
//	- Failed: Failed
//	- Not assessed: Failed
//
// Additionally, it writes a zip file to the body of the response.
//
// Returns an error if at least one test-case is classified as failed
// or if any error occurred during the analysis.
func analyseProtocols(w http.ResponseWriter, projectID id.ProjectID, caseLister handler.TestCaseLister, protocolLister handler.CaseProtocolLister, requestBody CIRequestBody) error {

	if !allTestsExecuted(projectID, caseLister, protocolLister, requestBody) {
		return errors.Construct(http.StatusNotFound).
			WithLog("not all tests executed").Finish()
	}

	cases, err := caseLister.List(projectID)
	if err != nil {
		return err
	}

	// Create writer for zip file
	zipBuffer := new(bytes.Buffer)
	zipWriter := zip.NewWriter(zipBuffer)

	var totalPassed int
	var totalAmount int
	for _, cs := range cases {
		version, ok := cs.TestCaseVersions[0].Versions[requestBody.Version]
		if !ok || !variantExists(version.Variants, requestBody.Variant) {
			// Skip this case if it is not applicable to the version & variant
			continue
		}

		totalAmount++

		caseProtocols, err := protocolLister.GetCaseExecutionProtocols(cs.ID())
		if err != nil {
			return err
		}

		applicableProtocol, ok := getNewestProtocol(caseProtocols, requestBody)
		if !ok {
			continue
		}

		if err = addProtocolToZip(zipWriter, applicableProtocol, cs); err != nil {
			return err
		}

		switch applicableProtocol.Result {
		case test.Fail:
			fallthrough
		case test.NotAssessed:
			continue
		case test.Pass:
			fallthrough
		case test.PartiallySuccessful:
			totalPassed++
		default:
			continue
		}
	}

	// writer is closed here because after this point the responses are sent
	if err = zipWriter.Close(); err != nil {
		return err
	}

	if totalAmount <= 0 {
		return errors.Construct(http.StatusNotFound).Finish()
	}

	if !hasPassed(totalAmount, totalPassed) {
		// Set the header of the response manually here because writing the zip-archive
		// into the body sets the header to 200 (OK) and it cannot be
		// overwritten afterwards. This behavior is documented in
		// https://golang.org/pkg/net/http/#ResponseWriter.
		// "If WriteHeader has not yet been called, Write calls
		// WriteHeader(http.StatusOK) before writing the data".
		errors.Construct(http.StatusBadRequest).Respond(w)
	}

	// Write zip to response-writer
	zipFileName := time.Now().UTC().String() + "-testresults-" + requestBody.Version + "-" + requestBody.Variant
	w.Header().Set("Content-Disposition", "attachment; filename="+zipFileName)
	_, err = io.Copy(w, zipBuffer)
	if err != nil {
		return errors.Construct(http.StatusInternalServerError).Finish()
	}

	return nil
}

// addProtocolToZip creates the pdf from a protocol and adds the pdf to the zip
func addProtocolToZip(zipWriter *zip.Writer, protocol test.CaseExecutionProtocol, testCase *test.Case) error {
	filename := protocol.TestVersion.Project() + "-" + protocol.TestVersion.Test() + "-" + strconv.Itoa(protocol.TestVersion.TestVersion()) + "-" + strconv.Itoa(protocol.ProtocolNr) + ".pdf"
	filename = strings.Replace(filename, ",", "", -1)
	fileWriter, err := zipWriter.Create(filename)
	if err != nil {
		return err
	}

	protocolReader, err := printing.BuildPdfForCaseProtocol(protocol, *testCase)
	if err != nil {
		return err
	}

	// Convert protocolReader to byte-array
	protocolBuffer := new(bytes.Buffer)
	protocolBuffer.ReadFrom(protocolReader)

	_, err = fileWriter.Write(protocolBuffer.Bytes())
	if err != nil {
		return err
	}

	return nil
}

// allTestsExecuted checks if all tests that are applicable to a given version
// of a version are executed.
//
// Returns true if there are tests and all tests are executed. Otherwise false.
func allTestsExecuted(projectID id.ProjectID, caseLister handler.TestCaseLister, protocolLister handler.CaseProtocolLister, requestBody CIRequestBody) bool {
	if requestBody.Version == "" || requestBody.Variant == "" {
		return false
	}

	cases, err := caseLister.List(projectID)
	if err != nil {
		return false
	}

	for _, cs := range cases {
		version, ok := cs.TestCaseVersions[0].Versions[requestBody.Version]
		if !ok || !variantExists(version.Variants, requestBody.Variant) {
			// Skip this case if it is not applicable to the version & variant
			continue
		}

		caseProtocols, err := protocolLister.GetCaseExecutionProtocols(cs.ID())
		if err != nil {
			return false
		}

		_, ok = getNewestProtocol(caseProtocols, requestBody)
		if !ok {
			return false
		}
	}

	return true
}

// getNewestProtocol returns the newest protocol for the given version AND variant
func getNewestProtocol(caseProtocols []test.CaseExecutionProtocol, requestBody CIRequestBody) (test.CaseExecutionProtocol, bool) {
	if caseProtocols == nil || len(caseProtocols) < 1 {
		return test.CaseExecutionProtocol{}, false
	}

	// Iterate reverse because the newest protocols are at the end of the slice
	for i := len(caseProtocols) - 1; i >= 0; i-- {
		if caseProtocols[i].SUTVersion == requestBody.Version && caseProtocols[i].SUTVariant == requestBody.Variant {
			return caseProtocols[i], true
		}
	}

	return test.CaseExecutionProtocol{}, false
}

// hasPassed evaluates whether the reached score is
// enough to return a successful response
func hasPassed(totalAmount, totalPassed int) bool {
	return totalAmount-totalPassed == 0
}
