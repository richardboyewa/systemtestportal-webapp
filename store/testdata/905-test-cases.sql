-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO test_cases (id, project_id, name)
VALUES
    (1, 1, 'Example test case'),
    (2, 2, 'Simple test suite'),
    (3, 2, 'Complete test suite with rules'),
    (4, 3, 'Deleted test case'),
    (5, 4, 'Change Theme'),
    (6, 4, 'Search for Websites'),
    (7, 4, 'Search for Images'),
    (8, 4, 'Search for Definitions'),
    (9, 4, 'Change Language'),
    (10, 4, 'Install DuckDuckGo.com for Chrome'),
    (11, 4, 'Change Font Size'),
    (12, 4, 'Map'),
    (13, 4, 'Enable Autosuggestion'),
    (14, 4, 'Search for Recipes'),
    (15, 4, 'Install DuckDuckGo.com for Firefox'),
    (16, 4, 'Install DuckDuckGo.com for Microsoft Edge'),
    (17, 5, 'Sign in'),
    (18, 5, 'Sign out'),
    (19, 5, 'Registration'),
    (20, 5, 'Create a Project'),
    (21, 5, 'Comment on a test case'), 
    (22, 5, 'Comment on a test sequence'),
    (23, 5, 'Create test case'),
    (24, 5, 'Add test steps'),
    (25, 5, 'Edit a test case'),
    (26, 5, 'Execute a test case'), 
    (27, 5, 'Create test sequence'), 
    (28, 5, 'Assign a test case'), 
    (29, 5, 'Assign a test sequence'), 
    (30, 5, 'View protocols'), 
    (31, 5, 'time execution'), 
    (32, 5, 'Versioning'), 
    (33, 5, 'Print');

-- +migrate Down
DELETE FROM test_cases;